import * as Location from "expo-location";
const URL = {
  CREATE_CURRENT_LOCATION:
    "https://sayhellotobike-native-ios-default-rtdb.firebaseio.com",
};

export const requestLocationPermission = async (
  setLocationCallBack,
  user_id,
  myCurrentLocationArray,
  setIsMapReady,
  
) => {
  try {
    const { status } = await Location.requestForegroundPermissionsAsync();
    if (status === "granted") {
      // 位置情報の取得処理を実行する
      getCurrentLocation(
        setLocationCallBack,
        user_id,
        myCurrentLocationArray,
        setIsMapReady,
      );
    } else {
      console.log("Location permission denied");
    }
  } catch (error) {
    console.error("Error requesting location permission:", error);
  }
};

export const getCurrentLocation = async (
  setLocationCallBack,
  user_id,
  myCurrentLocationArray,
  setIsMapReady,
  
) => {
  try {
    const { coords } = await Location.getCurrentPositionAsync({});
    const { latitude, longitude } = coords;
    setLocationCallBack({ ...myCurrentLocationArray, latitude, longitude });
    setIsMapReady(true); // マップが準備完了
    writeMyLocationData(URL.CREATE_CURRENT_LOCATION, user_id, latitude, longitude);
  } catch (error) {
    console.error("Error getting current location:", error);
  }
};

import { ref, set, onChildAdded } from "firebase/database";
import { database } from "../firebaseConfig"; // Firebaseのデータベースを正しくインポートする必要があります

export const writeMyLocationData = async (url, userId, latitude, longitude) => {
  console.log("書き込み開始:", url, userId, latitude, longitude);
  let params = {
    user_id: userId,
    latitude: latitude,
    longitude: longitude,
  };
  const path = `/user_id/${userId}`;

  try {
    // const locationRef = ref(database, url + '/location');
    const locationRef = ref(database, path);
    set(locationRef, params);
  } catch (error) {
    console.error(error);
  } finally {
    console.log("処理を送りました。");
  }
  console.log("fetchLocation", fetchLocation);
};
