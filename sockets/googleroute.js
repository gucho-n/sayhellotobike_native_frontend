const MAP_KEY = "AIzaSyCe2ZqMbYldfc-M7hXEBBSNJHnJy5gMig4";
import { saveTripsData } from "../firebaseConfig";
// 出発地と到着地
export const findPlace = async (placeId, MAP_KEY) => {
  const response = await fetch(
    `https://maps.googleapis.com/maps/api/place/details/json?place_id=${placeId}&fields=geometry&key=${MAP_KEY}`,
  );
  const data = await response.json();
  const { location } = data.result.geometry; // ジオメトリのlocationオブジェクトから緯度と経度を取得
  return { latitude: location.lat, longitude: location.lng }; // 緯度と経度が含まれる
};
export const fetchRoute = async (setOrigin, setDestination, routeInfo) => {
  let origin = routeInfo.origin;
  let destination = routeInfo.destination;
  let response = await fetch(
    `https://maps.googleapis.com/maps/api/directions/json?destination=${destination}&origin=${origin}&key=${MAP_KEY}`,
  );
  let routeData = await response.json();

  const originPlaceId = routeData.geocoded_waypoints[1].place_id;
  let fetchMyOrigin = await findPlace(originPlaceId, MAP_KEY);
  setOrigin(fetchMyOrigin);
  const destinationPlaceId = routeData.geocoded_waypoints[0].place_id;
  let fetchMyDestination = await findPlace(destinationPlaceId, MAP_KEY);
  setDestination(fetchMyDestination);
  saveTripsData(routeInfo);
};
