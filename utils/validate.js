const pattern = {
  email: /^[a-zA-Z0-9._%+-@]/,
  password: /^[a-zA-Z0-9._%+-@]/,
};
const comment = {
  email: "メールアドレスの形に直してください",
  password: "パスワードは半角英数字で入力してください",
};

export const checkValidate = (
  inputData,
  pattern,
  comment,
  setValidationMessages,
) => {
  if (pattern.test(inputData)) {
    setValidationMessages(comment);
    return;
  }
  setValidationMessages([""]);
  return;
};
export const checkEmailValidate = (inputData, setValidationMessages) => {
  if (!pattern.email.test(inputData)) {
    setValidationMessages(comment.email);
    return;
  }
  setValidationMessages([""]);
  return;
};

export const checkPasswordValidate = (inputData, setValidationMessages) => {
  if (!pattern.password.test(inputData)) {
    setValidationMessages(comment.password);
    return;
  }
  setValidationMessages([""]);
  return;
};
