export const fetchGETAPI = async (url, header, query) => {
  try {
    let querySet = query.map((e) => "?" + e);
    let urlWithQuery = url + querySet.join("");
    let response = await fetch(urlWithQuery, {
      headers: header,
    });
    return response;
  } catch (error) {
    console.error("通信がうまくいきませんでした。", error);
    throw error; // エラーを呼び出し元に投げる
  } finally {
    console.log("処理が完了しました");
  }
};

export const fetchPOSTAPI = async (url, header, body) => {
  try {
    let response = await fetch(url, {
      method: "POST",
      headers: header,
      body: JSON.stringify(body),
    });
    return response;
  } catch (error) {
    console.error("通信がうまくいきませんでした。", error);
    throw error; // エラーを呼び出し元に投げる
  } finally {
    console.log("処理が完了しました");
  }
};
