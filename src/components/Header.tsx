import React, { useState } from "react";
import { View, StyleSheet } from "react-native";
import ToggleButton from "./ToggleButton";
import Navigation from "./Navigation";

const Header = () => {
  const [open, setOpen] = useState(false);

  const toggleFunction = () => {
    setOpen((prevState) => !prevState);
  };

  return (
    <View style={styles.header}>
      <ToggleButton
        open={open}
        label="メニューを開きます"
        onPress={toggleFunction}
      />
      <Navigation id="navigation" open={open} />
    </View>
  );
};

const styles = StyleSheet.create({
  header: {
    flexDirection: "row",
    justifyContent: "space-between",
    alignItems: "center",
    padding: 10,
    backgroundColor: "#ccc",
  },
});

export default Header;
