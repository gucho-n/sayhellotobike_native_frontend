import React from "react";
import { TouchableOpacity, View, Text, StyleSheet } from "react-native";
import "./styles.css";
type Props = {
  open: boolean;
  onPress: () => void;
  label: string;
};

const ToggleButton: React.FC<Props> = ({ open, onPress, label }) => {
  return (
    <TouchableOpacity onPress={onPress} style={styles.toggleButton}>
      <View>
        <Text>{label}</Text>
      </View>
    </TouchableOpacity>
  );
};

const styles = StyleSheet.create({
  toggleButton: {
    padding: 10,
    backgroundColor: "#ccc",
    borderRadius: 5,
  },
});

export default ToggleButton;
