// Navigation.tsx

import React from "react";
import { View, Text } from "react-native";

type Props = {
  open: boolean;
  id: string;
};

const Navigation: React.FC<Props> = ({ open, id }) => {
  return (
    <View accessibilityRole="navigation" id={id}>
      {open ? (
        <View style={styles.navigation}>
          <Text>about</Text>
          <Text>works</Text>
          <Text>contact</Text>
        </View>
      ) : null}
    </View>
  );
};

const styles = {
  navigation: {
    // Add your styles for navigation here
  },
};

export default Navigation;
