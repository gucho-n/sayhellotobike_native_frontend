import React, { useState } from "react";
import { navigationProps } from "./navigationProps/navigationProps";
import { useNavigation } from "@react-navigation/native"; // useNavigationを追加

// 1.画面の遷移には以下をインポートする
import { NavigationContainer } from "@react-navigation/native";
import { createNativeStackNavigator } from "@react-navigation/native-stack";

import Top from "./Top";
import Main from "./Main";
import Register from "./Register";
// パラメータはTOPの(params)
const Top = () => {
  const navigation = useNavigation(); // useNavigationフックを使用してnavigationオブジェクトを取得
  const Stack = createNativeStackNavigator();

  return (
    <>
      <Stack.Navigator initialRouteName="Top" screenOptions={navigationProps}>
        <Stack.Screen
          name="Top"
          component={Top}
          options={{
            title: "Top",
            headerShown: false,
            headerBackTitleVisible: false,
          }}
        />
        <Stack.Screen
          name="Main"
          component={Main}
          options={{
            title: "メイン",
            headerShown: false,
            headerBackTitleVisible: false,
          }}
        />
        <Stack.Screen
          name="Register"
          component={Register}
          options={{
            title: "登録",
            headerShown: false,
            headerBackTitleVisible: false,
          }}
        />
      </Stack.Navigator>
    </>
  );
};

export default Top;
