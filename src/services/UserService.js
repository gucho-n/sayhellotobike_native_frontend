// services/UserService.js

import axios from "axios";
import { USER_URL } from "../constants/API";

export const getUser = async (userId) => {
  try {
    const response = await axios.get(`${USER_URL}/${userId}`);
    return response.data;
  } catch (error) {
    console.error("Error while fetching user:", error);
    throw error;
  }
};
