// constants/API.js

const BASE_URL = "https://example.com/api";

export const USER_URL = `${BASE_URL}/users`;
export const POST_URL = `${BASE_URL}/posts`;
// 他のエンドポイントのURLもここで定義する
