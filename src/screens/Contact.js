import React from "react";
import {
  View,
  StyleSheet,
  Text,
  ScrollView,
  TextInput,
  TouchableOpacity,
} from "react-native";
import { useForm, Controller } from "react-hook-form";

const Contact = () => {
  const {
    control,
    handleSubmit,
    formState: { errors },
  } = useForm();

  const onSubmit = (data) => {
    const api_url = "apiのURL";
    fetch(api_url, {
      method: "post",
      headers: {
        "Content-Type": "application/x-www-form-urlencoded",
      },
      body: encodeURI(
        `name=${data.name}&email=${data.email}&body=${data.body}`,
      ),
    })
      .then((response) => response.json())
      .then((result) => alert(result.message))
      .catch((error) => alert(error.message));
  };
  return (
    <ScrollView>
      <View style={styles.Content}>
        <Text style={styles.titleText}>お問い合わせ</Text>
        <View style={styles.formWrapper}>
          <Text>お名前</Text>
          <Controller
            name="name"
            control={control}
            rules={{
              required: "お名前は必須です。",
            }}
            render={({ field: { onChange, onBlur, value } }) => (
              <TextInput
                placeholder="お名前"
                numberOfLines={1}
                style={styles.textInput}
                autoCapitalize="none"
                onBlur={onBlur}
                onChangeText={onChange}
                value={value}
              />
            )}
          />
          {errors.name && (
            <Text style={styles.errorMessage}>{errors.name.message}</Text>
          )}

          <Text>Email</Text>
          <Controller
            name="email"
            control={control}
            rules={{
              required: "emailは必須です。",
              pattern: {
                value: /^[a-z0-9.]+@[a-z0-9.]+\.[a-z]+$/,
                message: "emailは必須かつemailの形式で入力してください。",
              },
            }}
            render={({ field: { onChange, onBlur, value } }) => (
              <TextInput
                placeholder="Email"
                numberOfLines={1}
                style={styles.textInput}
                autoCapitalize="none"
                onBlur={onBlur}
                onChangeText={onChange}
                value={value}
              />
            )}
          />
          {errors.email && (
            <Text style={styles.errorMessage}>{errors.email.message}</Text>
          )}

          <Text>お問合せ内容</Text>
          <Controller
            name="body"
            control={control}
            rules={{
              required: "お問い合わせは必須です。",
              maxLength: {
                value: 10,
                message:
                  "お問合せ内容は必須かつ1文字以上10文字以下で入力してください。",
              },
            }}
            render={({ field: { onChange, onBlur, value } }) => (
              <TextInput
                placeholder="お問合せ内容"
                multiline={true}
                style={[styles.textArea]}
                onBlur={onBlur}
                onChangeText={onChange}
                value={value}
              />
            )}
          />
          {errors.body && (
            <Text style={styles.errorMessage}>{errors.body.message}</Text>
          )}
          <TouchableOpacity
            onPress={handleSubmit(onSubmit)}
            style={styles.submitBtn}
          >
            <Text>送信</Text>
          </TouchableOpacity>
        </View>
      </View>
    </ScrollView>
  );
};

export default Contact;

const styles = StyleSheet.create({
  Content: {
    flex: 1,
    alignItems: "center",
    justifyContent: "center",
  },
  titleText: {
    fontWeight: "bold",
    paddingBottom: 10,
    paddingTop: 60,
  },
  formWrapper: {
    width: 300,
  },
  textInput: {
    backgroundColor: "#00000011",
    paddingHorizontal: 5,
    height: 30,
    marginVertical: 10,
  },
  textArea: {
    backgroundColor: "#00000011",
    paddingHorizontal: 5,
    height: 60,
    marginVertical: 10,
  },
  errorMessage: {
    marginBottom: 15,
    fontSize: 10,
    color: "red",
  },
  submitBtn: {
    alignItems: "center",
    justifyContent: "center",
    backgroundColor: "#00000033",
    height: 50,
  },
});
