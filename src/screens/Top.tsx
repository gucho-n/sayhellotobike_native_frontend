import React, { useState } from "react";
import {
  View,
  Text,
  ScrollView,
  ImageBackground,
  StyleSheet,
  TextInput,
  Alert,
} from "react-native";
import { useNavigation } from "@react-navigation/native"; // useNavigationを追加
import { createNativeStackNavigator } from "@react-navigation/native-stack";
import {
  checkEmailValidate,
  checkPasswordValidate,
} from "../../utils/validate";
import { signInWithEmailAndPassword } from "firebase/auth";
import { auth } from "../../firebaseConfig";

import { Button, Input } from "react-native-elements"; // React Native Elementsからボタンをインポート
// パラメータはTOPの(params)
const Top = () => {
  const navigation = useNavigation(); // useNavigationフックを使用してnavigationオブジェクトを取得
  const Stack = createNativeStackNavigator();
  const [email, setEmail] = useState(""); // emailの状態とその更新関数setEmailを定義
  const [password, setPassword] = useState(""); // emailの状態とその更新関数setEmailを定義
  const [validate, setValidationMessages] = useState([]); // emailの状態とその更新関数setEmailを定義
  // メールチェック
  const handleEmail = (input: string) => {
    setEmail(input);
    checkEmailValidate(input, setValidationMessages);
    return;
  };
  const handlePassword = (input: string) => {
    setPassword(input);
    checkPasswordValidate(input, setValidationMessages);
    return;
  };
  const handleSignIn = async (auth, email, password) => {
    try {
      let result = await signInWithEmailAndPassword(auth, email, password);
      const myUserNumber = result.user.uid;
      navigation.navigate("Main", { screens: "Main", user_id: myUserNumber });
    } catch (error) {
      Alert.alert(
        "ログインエラー",
        "サインインに失敗しました。正しいメールアドレスとパスワードを入力してください。",
      );
    } finally {
      console.log("確認が完了しました");
    }
  };

  return (
    <>
      <ScrollView>
        <Text style={styles.titleText}>SayHelloToBike</Text>
        <Text style={styles.subTitle}>バイクでつながるコミュニケーション</Text>
        <ImageBackground
          source={require("../../assets/bike.png")}
          style={styles.Top}
        ></ImageBackground>
        <View style={styles.imputWrapper}>
          <Text>{validate}</Text>
          <View style={styles.imputContainer}>
            <Text>メールアドレス</Text>
            <Input
              placeholder={"メールアドレスを入力してください"}
              value={email}
              autoCapitalize="none"
              onChangeText={handleEmail}
              inputStyle={styles.inputBox}
            />
          </View>
          <View style={styles.imputContainer}>
            <Text>パスワード</Text>
            <Input
              placeholder="パスワードを入力してください"
              value={password}
              autoCapitalize="none"
              onChangeText={(text) => handlePassword(text)}
              inputStyle={styles.inputBox} // スタイルは inputStyle で指定します
            />
          </View>
          <View>
            <Button
              title="LOG IN"
              onPress={() => handleSignIn(auth, email, password)}
              buttonStyle={{
                backgroundColor: "black",
                borderWidth: 2,
                borderColor: "white",
                borderRadius: 30,
              }}
              containerStyle={{
                width: 300,
                marginHorizontal: 50,
                marginVertical: 10,
              }}
              titleStyle={{ fontWeight: "bold" }}
            />
            {/* <Button title="サインイン" onPress={() => handleSignIn(email, password)} /> */}
          </View>
          <View>
            <Button
              title="新規登録はこちらから"
              onPress={() =>
                navigation.navigate("Register", { screens: "Register" })
              }
              // disabled={true}
              titleStyle={{ fontWeight: "700" }}
              buttonStyle={{
                backgroundColor: "rgba(169, 169, 169, 0.5)", // 透明なグレー
                borderColor: "transparent",
                borderWidth: 0,
                borderRadius: 5,
              }}
              containerStyle={{
                width: 200,
                height: 45,
                marginHorizontal: 50,
                marginVertical: 10,
              }}
            />
          </View>
        </View>
      </ScrollView>
    </>
  );
};

export default Top;

const styles = StyleSheet.create({
  titleText: {
    color: "black",
    fontWeight: "bold",
    fontSize: 24,
    textAlign: "center",
    padding: 20,
  },
  subTitle: {
    fontSize: 14,
    textAlign: "center",
    padding: 5,
  },
  Top: {
    flex: 1,
    alignItems: "center",
    justifyContent: "center",
    fontSize: 20,
    paddingTop: 100,
    paddingBottom: 60,
  },
  imputWrapper: {
    alignItems: "center", // 横方向に中央揃え
    padding: 20,
  },
  imputContainer: {
    width: "100%",
    marginBottom: 10,
  },
  imputBox: {
    height: 40,
    borderWidth: 1,
  },
});
