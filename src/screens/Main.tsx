import React, { useState, useEffect } from "react";
import {
  View,
  ActivityIndicator,
  Text,
  ScrollView,
  StyleSheet,
  TextInput,
  Button,
  Switch,
  Dimensions,
  Image,
  Alert,
} from "react-native";
import { useNavigation, useRoute } from "@react-navigation/native"; // useNavigationを追加
import MapView, { Marker, Polyline } from "react-native-maps";
import {
  requestLocationPermission,
  getCurrentLocation,
  writeMyLocationData,
} from "../../sockets/googlemap";
import { findPlace, fetchRoute } from "../../sockets/googleroute";
import { ref, onChildAdded } from "firebase/database";
import { database, firestore, getMyUserData,getMyImage } from "../../firebaseConfig"; // Firebaseのデータベースを正しくインポートする必要があります
import { Input } from "react-native-elements"; // React Native Elementsからボタンをインポート
import {
  doc,
  setDoc,
  collection,
  addDoc,
  getDocs,
  getDoc,
} from "firebase/firestore";

const Main = () => {
  const [otherRiders, setOtherRiders] = useState([]);

  const dbref = ref(database); //取得したいデータのパスを指定
  const [routeInfo, setRouteInfo] = useState({ origin: "", destination: "" });
  const navigation = useNavigation(); // useNavigationフックを使用してnavigationオブジェクトを取得
  const route = useRoute();
  const user_id = route.params?.user_id;
  const [myCurrentLocation, setMyCurrentLocation] = useState({
    latitude: 0,
    longitude: 0,
  });
  const [myDestination, setMyDestination] = useState({
    latitude: 0,
    longitude: 0,
  });
  const [myOrigin, setMyOrigin] = useState({
    latitude: 0,
    longitude: 0,
  });
  // DB を取得している

  const [isMapReady, setIsMapReady] = useState(false);
  const [isEnabled, setIsEnabled] = useState(false);
  const [iconImage, setImageUri] = useState<string | null>(null); // 型を string | null に修正
  const toggleSwitch = () => setIsEnabled((previousState) => !previousState);
//   iconImage = require('../../assets/kurumi.jpg');

  const myDestinationIcon = require("../../assets/flag.png");
  const myOriginIcon = require("../../assets/start.png");

  // requestLocationPermission();
  useEffect(() => {
    requestLocationPermission(
      setMyCurrentLocation,
      user_id,
      myCurrentLocation,
      setIsMapReady,
      
    );
    const fetchData = async () => {
      try {
        const fetchMyUserData = await getMyUserData(user_id);
        console.log("aaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaa",fetchMyUserData)
        const fetchMyImage = await getMyImage(user_id);
        console.log("fetchMyImage",fetchMyImage)       
        setImageUri(fetchMyImage);
      } catch (error) {
        console.error("Error fetching image: ", error);
      }
    };
    fetchData();
    const childAddedListener = onChildAdded(dbref, function (snapshot) {
      let data = snapshot.val();
      setOtherRiders(data);
    });

    return () => {
      // コンポーネントがアンマウントされたときにイベントリスナーを解除する
      childAddedListener();
    };
  }, []);

  return isMapReady ? ( // マップが準備完了したら表示
    <ScrollView style={styles.Wrapper}>
      <View style={styles.mapContainer}>
        <MapView
          style={styles.map}
          initialRegion={{
            latitude: myCurrentLocation.latitude,
            longitude: myCurrentLocation.longitude,
            latitudeDelta: 0.0922,
            longitudeDelta: 0.0421,
          }}
        >
            <Marker
                coordinate={{
                    latitude: myCurrentLocation.latitude,
                    longitude: myCurrentLocation.longitude,
                }}
            >
            <Image style={styles.icon} source={{ uri: iconImage }} />
          </Marker>
          <Marker
            coordinate={{
              latitude: myOrigin.latitude,
              longitude: myOrigin.longitude,
            }}
          >
            <Image style={styles.icon} source={myOriginIcon} />
          </Marker>
          <Marker
            coordinate={{
              latitude: myDestination.latitude,
              longitude: myDestination.longitude,
            }}
          >
            <Image style={styles.icon} source={myDestinationIcon} />
          </Marker>
          <Polyline
            coordinates={[
              { latitude: myOrigin.latitude, longitude: myOrigin.longitude },
              {
                latitude: myDestination.latitude,
                longitude: myDestination.longitude,
              },
            ]}
            strokeColor="#FF0000" // 線の色
            strokeWidth={2} // 線の太さ
          />
          {Array.isArray(otherRiders) && otherRiders.length > 0
            ? otherRiders.map((rider, index) => {
                console.log("rider", rider);
                let diferLat =
                  Math.abs(myCurrentLocation.latitude - rider.latitude) < 1;
                let diferLot =
                  Math.abs(myCurrentLocation.longitude - rider.longitude) < 1;
                // if(diferLat && diferLot){
                //     Alert.alert("手を振りましょう")
                // }
                return (
                  <Marker
                    key={index} // ここで一意のキーを提供する
                    coordinate={{
                      latitude: rider.latitude,
                      longitude: rider.longitude,
                    }}
                  >
                    <Image style={styles.icon} source={iconImage} />
                  </Marker>
                );
              })
            : null}
        </MapView>
      </View>
      <Text style={styles.direction}>出発地</Text>
      <Input
        placeholder={"出発地を入力してください"}
        value={routeInfo.origin}
        onChangeText={(text) => setRouteInfo({ ...routeInfo, origin: text })}
      />
      <Text style={styles.direction}>目的地</Text>
      <Input
        placeholder={"到着地を入力してください"}
        value={routeInfo.destination}
        onChangeText={(text) =>
          setRouteInfo({ ...routeInfo, destination: text })
        }
      />
      <View>
        <Button
          title="ルートを検索する"
          onPress={() => fetchRoute(setMyOrigin, setMyDestination, routeInfo)}
        />
      </View>
      <View>
        <Button title="戻る" onPress={() => navigation.navigate("Top")} />
      </View>
    </ScrollView>
  ) : (
    <View style={styles.waitContainer}>
      <ActivityIndicator size="large" color="#0000ff" />
      <Text style={styles.waitText}>少々お待ちください...</Text>
    </View>
  );
};

export default Main;

const { height } = Dimensions.get("window");
const mapHeight = height * 0.5; // 画面の半分の高さ
const styles = StyleSheet.create({
  Wrapper: {
    flex: 1, // 画面の半分のサイズに設定
  },
  direction: {
    fontSize: 12,
    padding: 10,
  },
  infomation: {
    fontSize: 12,
    padding: 10,
  },
  subTitle: {
    fontSize: 14,
    textAlign: "center",
    padding: 5,
  },
  mapContainer: {
    height: mapHeight,
    justifyContent: "center",
    alignItems: "center",
  },
  map: {
    ...StyleSheet.absoluteFillObject,
  },
  markerContainer: {
    backgroundColor: "blue",
    padding: 5,
    borderRadius: 5,
  },
  markerText: {
    color: "white",
    fontWeight: "bold",
  },
  imputWrapper: {
    alignItems: "center", // 横方向に中央揃え
    padding: 20,
  },
  imputContainer: {
    width: "100%",
    marginBottom: 10,
  },
  imputBox: {
    height: 40,
    borderWidth: 1,
  },
  icon: {
    width: 28,
    height: 28,
  },
  waitScrollViewContent: {
    flex: 1,
    width: "100%",
  },
  center: {
    flex: 1,
    justifyContent: "center",
    alignItems: "center",
  },
  waitContainer: {
    flex: 1,
    justifyContent: "center",
    alignItems: "center",
  },
  waitText: {
    marginTop: 10,
    fontSize: 16,
  },
});
