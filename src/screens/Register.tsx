import React, { useState } from "react";
import { View, Text, ScrollView, StyleSheet, Image } from "react-native";
import { useNavigation } from "@react-navigation/native";
import { Button } from "react-native-elements";
import { Input } from "@rneui/themed";
import { createUserWithEmailAndPassword } from "firebase/auth";
import { auth } from "../../firebaseConfig";
import {
  checkEmailValidate,
  checkPasswordValidate,
} from "../../utils/validate";
import { fetchPOSTAPI } from "../../utils/API";
import { saveUserData ,saveImage} from "../../firebaseConfig";
import * as ImagePicker from "expo-image-picker";

const Register = () => {
  const navigation = useNavigation();
  const [registerDatas, setRegisterDatas] = useState({
    nickName: "",
    email: "",
    password: "",
    passwordConfirm: "",
    range: "",
    image: null,
  });
  const [validate, setValidationMessages] = useState([]);

  const handleNickName = (input) => {
    setRegisterDatas({ ...registerDatas, nickName: input });
  };

  const handleEmail = (input) => {
    setRegisterDatas({ ...registerDatas, email: input });
    checkEmailValidate(input, setValidationMessages);
  };

  const handlePassword = (input) => {
    setRegisterDatas({ ...registerDatas, password: input });
    checkPasswordValidate(input, setValidationMessages);
  };

  const handlePasswordConfirm = (input) => {
    setRegisterDatas({ ...registerDatas, passwordConfirm: input });
    checkPasswordValidate(input, setValidationMessages);
  };

  const handleRange = (input) => {
    setRegisterDatas({ ...registerDatas, range: input });
    checkPasswordValidate(input, setValidationMessages);
  };

  const handleSignUp = async (auth, email, password, registerDatas) => {
    try {
      let successedAccount = await createUserWithEmailAndPassword(
        auth,
        email,
        password,
      );
      const myUserNumber = successedAccount.user.uid;
      saveUserData(registerDatas, myUserNumber);
      saveImage(registerDatas, myUserNumber);
      console.log(
        "User account created & signed in!",
        successedAccount.user.uid,
      );
      navigation.navigate("Top", { screens: "Top", user_id: myUserNumber });
    } catch (error) {
      console.error("Error signing up:", error);
    } finally {
      console.log("ログインの判定が完了しました。");
    }
  };

  const pickImage = async () => {
    let result = await ImagePicker.launchImageLibraryAsync({
      mediaTypes: ImagePicker.MediaTypeOptions.Images,
      allowsEditing: true,
      aspect: [4, 3],
      quality: 1,
    });

    if (!result.canceled) {
      setRegisterDatas({ ...registerDatas, image: result.assets[0].uri });
    }
  };

  return (
    <ScrollView>
      <Text style={styles.titleText}>SayHelloToBike</Text>
      <Text style={styles.subTitle}>登録する</Text>
      <View style={styles.inputWrapper}>
        <Text>{validate}</Text>
        <View style={styles.inputContainer}>
          <Text>ニックネーム</Text>
          <Input
            placeholder="ニックネームを入力してください"
            value={registerDatas.nickName}
            onChangeText={handleNickName}
            inputStyle={styles.inputBox}
            inputContainerStyle={styles.inputContainer}
          />
        </View>
        <View style={styles.inputContainer}>
          <Text>メールアドレス</Text>
          <Input
            placeholder="メールアドレスを入力してください"
            value={registerDatas.email}
            onChangeText={handleEmail}
            inputStyle={styles.inputBox}
            inputContainerStyle={styles.inputContainer}
          />
        </View>
        <View style={styles.inputContainer}>
          <Text>パスワード</Text>
          <Input
            placeholder="パスワードを入力してください"
            value={registerDatas.password}
            onChangeText={handlePassword}
            secureTextEntry={true}
            inputStyle={styles.inputBox}
            inputContainerStyle={styles.inputContainer}
          />
        </View>
        <View style={styles.inputContainer}>
          <Text>パスワード(確認)</Text>
          <Input
            placeholder="パスワードを入力してください"
            value={registerDatas.passwordConfirm}
            onChangeText={handlePasswordConfirm}
            secureTextEntry={true}
            inputStyle={styles.inputBox}
            inputContainerStyle={styles.inputContainer}
          />
        </View>
        <View style={styles.inputContainer}>
          <Text>通知距離</Text>
          <Input
            placeholder="通知距離"
            value={registerDatas.range}
            onChangeText={handleRange}
            inputStyle={styles.inputBox}
            inputContainerStyle={styles.inputContainer}
          />
        </View>
        <View style={styles.inputContainer}>
          <Text>プロフィール画像</Text>
          <Button title="画像を選択" onPress={pickImage} />
          {registerDatas.image && (
            <Image
              source={{ uri: registerDatas.image }}
              style={{ width: 200, height: 200 }}
            />
          )}
        </View>
        <View>
          <Button
            title="登録する"
            onPress={() =>
              handleSignUp(
                auth,
                registerDatas.email,
                registerDatas.password,
                registerDatas,
              )
            }
            buttonStyle={styles.buttonStyle}
            containerStyle={styles.buttonContainer}
            titleStyle={styles.buttonTitle}
          />
        </View>
        <View>
          <Button
            title="戻る"
            onPress={() => navigation.navigate("Top")}
            buttonStyle={styles.buttonStyle}
            containerStyle={styles.buttonContainer}
            titleStyle={styles.buttonTitle}
          />
        </View>
      </View>
    </ScrollView>
  );
};

export default Register;

const styles = StyleSheet.create({
  titleText: {
    color: "black",
    fontWeight: "bold",
    fontSize: 24,
    textAlign: "center",
    padding: 20,
  },
  subTitle: {
    fontSize: 14,
    textAlign: "center",
    padding: 5,
  },
  inputWrapper: {
    alignItems: "center",
    padding: 20,
  },
  inputContainer: {
    width: "100%",
    marginBottom: 10,
  },
  inputBox: {
    height: 40,
    borderWidth: 1,
  },
  buttonStyle: {
    backgroundColor: "black",
    borderRadius: 30,
  },
  buttonContainer: {
    width: 300,
    marginHorizontal: 50,
    marginVertical: 10,
  },
  buttonTitle: {
    fontWeight: "bold",
  },
});
