import React from "react";
// 1.画面の遷移には以下をインポートする
import { NavigationContainer } from "@react-navigation/native";
import { createBottomTabNavigator } from "@react-navigation/bottom-tabs";
import { MaterialCommunityIcons } from "@expo/vector-icons";
// 2.遷移するページはここにインポートしている
import Top from "./src/screens/Top";
import Contact from "./src/screens/Contact";
// 3.遷移するページはここにインポートしている
const Tab = createBottomTabNavigator();
// https://reffect.co.jp/react/react-navigation

import { createNativeStackNavigator } from "@react-navigation/native-stack";

import Main from "./src/screens/Main";

export default function App() {
  const Stack = createNativeStackNavigator();
  return (
    <NavigationContainer>
      <Tab.Navigator
        screenOptions={{
          headerStyle: {
            backgroundColor: "#666",
          },
          headerTintColor: "#fff",
          headerTitleStyle: {
            fontWeight: "bold",
          },
          tabBarStyle: {
            backgroundColor: "#666",
          },
          tabBarInactiveTintColor: "#ffffff88",
          tabBarActiveTintColor: "#fff",
        }}
      >
        <Tab.Screen
          name="Top"
          component={Top}
          options={{
            title: "TOP",
            tabBarIcon: ({ color, size }) => (
              <MaterialCommunityIcons name="home" color={color} size={size} />
            ),
          }}
        />
        <Tab.Screen
          name="Contact"
          component={Contact}
          options={{
            title: "お問合せ",
            tabBarIcon: ({ color, size }) => (
              <MaterialCommunityIcons
                name="information"
                color={color}
                size={size}
              />
            ),
          }}
        />
      </Tab.Navigator>
    </NavigationContainer>
  );
}
