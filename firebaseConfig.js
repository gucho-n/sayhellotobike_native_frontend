import { initializeApp } from "firebase/app";
import { getAuth } from "firebase/auth";
import { getDatabase } from "firebase/database";
import { getStorage, ref, uploadBytes,getDownloadURL } from "firebase/storage";
import {
  initializeFirestore,
  CACHE_SIZE_UNLIMITED,
  getFirestore,
  collection,
  experimentalAutoDetectLongPolling,
  enableNetwork,
  onSnapshot,
  doc,
  addDoc,
  getDocs,
  getDoc,
  setDoc,
} from "firebase/firestore";

const firebaseConfig = {
  apiKey: "AIzaSyCe2ZqMbYldfc-M7hXEBBSNJHnJy5gMig4",
  authDomain: "sayhellotobike-native-ios.firebaseapp.com",
  databaseURL: "https://sayhellotobike-native-ios-default-rtdb.firebaseio.com",
  projectId: "sayhellotobike-native-ios",
  storageBucket: "sayhellotobike-native-ios.appspot.com",
  messagingSenderId: "94237205998",
  appId: "1:94237205998:web:879feeef3ddb781d3e1aff",
  measurementId: "G-ZLR4NQ9XG4",
};

const app = initializeApp(firebaseConfig);
export const auth = getAuth(app);
export const database = getDatabase(app);
export const firestore = initializeFirestore(
  app,
  firebaseConfig,
  "sayhellotobike-native-backend",
);

const storage = getStorage(app);



export const saveUserData = async (userData,user_id) => {
  try {
    // const userRef = await addDoc(collection(firestore, `${user_id}`), userData);
    // console.log("Document written with ID: ", userRef.id);
    // ドキュメントリファレンス (偶数セグメント)

    const userDocRef = await setDoc(doc(firestore, "users", user_id), {
        nickName: userData.nickName,
        range: userData.range,
        date: "2024-05-20",
      });
  } catch (e) {
    console.error("Error adding document: ", e);
  } finally {
    console.log("保存が完了しました！");
  }
};


export const saveImage = async (userData, userId) => {
  try {
    const imageUri = userData.image;
    const imageName = imageUri.split('/').pop(); // 画像名を取得
    const type = imageUri.split('.').pop(); // ファイルの種類を取得
    const mountainsRef = ref(storage, `${userId}/image`);
    const response = await fetch(imageUri);
    const blob = await response.blob();
    let snapshot = await uploadBytes(mountainsRef, blob);
    console.log("Firestoreに画像情報を保存しました。",snapshot);

    // アップロードした画像のURLを取得
    const downloadURL = await getDownloadURL(mountainsRef);
    console.log('Download URL: ', downloadURL);

    // Firestoreに画像情報を保存
    const userDocRef = doc(firestore, `users/${userId}`);
    await setDoc(userDocRef, { imageUrl: downloadURL }, { merge: true });

    console.log("Firestoreに画像情報を保存しました。", snapshot);

  } catch (e) {
    console.error("Error adding document: ", e);
  } finally {
    console.log("saveImage");
  }
};

export const saveTripsData = async (tripData) => {
  try {
    // サブコレクションにドキュメントを追加
    const TripsRef = await addDoc(
      collection(firestore, `users/${userDocId}/trips`),
      {
        origin: tripData.origin,
        destination: tripData.destination,
      },
    );
    console.log("Trip document written with ID: ", TripsRef);
    console.log("Document written with fixed ID: ", TripsRef.id);
  } catch (e) {
    console.error("saveTripsData: ", e);
  } finally {
    console.log("保存が完了しました！");
  }
};
// };
// 関数を呼び出す
// 画像を取得する関数
export const getMyUserData = async (userDocId) => {
  try {
    if (!userDocId) {
      console.error("Error: userDocId is null");
      return null; // nullを返す
    }
   
    const docRef = doc(firestore, "users", userDocId);
    const docSnap = await getDoc(docRef);
    if (docSnap.exists()) {
      return docSnap.data(); // ドキュメントデータを返す
    } else {
      console.error("No such document!");
      return null; // ドキュメントが存在しない場合はnullを返す
    }
  } catch (error) {
    console.error("Error fetching image: ", error);
    throw error;
  }
};



export const getMyImage = async (userId) => {
  try {
    if (!userId) {
      throw new Error("Invalid userId");
    }

    // ストレージの参照を取得
    const userDocRef = ref(storage, `${userId}/image`);

    // 画像のダウンロードURLを取得
    const downloadURL = await getDownloadURL(userDocRef);
    console.log("TEST", downloadURL);
    // This can be downloaded directly:
    const xhr = new XMLHttpRequest();
    xhr.responseType = 'blob';
    xhr.onload = (event) => {
      const blob = xhr.response;
    };
    xhr.open('GET', downloadURL);
    xhr.send()

    // URLを返す
    return downloadURL;

  } catch (error) {
    console.error("getMyImage: ", error);
    throw error;
  }
};
