import React from "react";
// 1.画面の遷移には以下をインポートする
import { NavigationContainer } from "@react-navigation/native";
import { createBottomTabNavigator } from "@react-navigation/bottom-tabs";
// 2.遷移するページはここにインポートしている
import Top from "./src/screens/Top";
// 3.遷移するページはここにインポートしている
const Tab = createBottomTabNavigator();
// https://reffect.co.jp/react/react-navigation

import { createNativeStackNavigator } from "@react-navigation/native-stack";
import Main from "./src/screens/Main";
import Register from "./src/screens/Register";

export default function App() {
  // // firebaseの設定について
  // 1.最低限の設定としてapiKey/projectIdを接続する
  // 2.外部認証が必要だった場合、authDomainも設定する

  // https://console.firebase.google.com/project/sayhellotobike-native-ios/settings/general?hl=ja

  // ルーティングの設定
  const Stack = createNativeStackNavigator();
  return (
    <NavigationContainer>
      <Stack.Navigator initialRouteName="Top">
        <Stack.Screen
          name="Top"
          component={Top}
          options={{
            title: "Top",
            headerShown: false,
            headerBackTitleVisible: false,
          }}
        />
        <Stack.Screen
          name="Main"
          component={Main}
          options={{
            title: "メイン",
            headerShown: false,
            headerBackTitleVisible: false,
          }}
        />
        <Stack.Screen
          name="Register"
          component={Register}
          options={{
            title: "新規登録",
            headerShown: false,
            headerBackTitleVisible: false,
          }}
        />
      </Stack.Navigator>
    </NavigationContainer>
  );
}
